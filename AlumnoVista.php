<?php

abstract class AlumnoVista {

    public static function mostrarListado($alumnos) 
    {
        $retorno = '<ul>';
        foreach ($alumnos as $alumno) {
            $retorno .= '<li>' . $alumno['nombre'] . ' ' . $alumno['apellidos'];
            $retorno .= " <a href='?modulo=detalle&id=" . $alumno['id'] . "'>Mostrar</a>";
            $retorno .= " <a href='?modulo=modificaralumno&id=" . $alumno['id'] . "'>Modificar</a>";
            $retorno .= " <a href='?modulo=eliminar&id=" . $alumno['id'] . "'>Eliminar</a>";
            $retorno .= "</li>";
        }
        $retorno .= "<li><a href='?modulo=nuevoalumno'> Nuevo alumno </a></li>";
        $retorno .='</ul>';
        echo $retorno;
    }

    public function mostrarDetalle(Alumno $alumno) 
            {
        $retorno = '<h1> Datos del alumno </h1>';
        $retorno .= 'Nombre:' . $alumno->getNombre() . '<br>';
        $retorno .= 'Apellidos:' . $alumno->getApellidos() . '<br>';
        $retorno .= 'Edad:' . $alumno->getEdad() . '<br>';
        
        echo $retorno;
            }
            
            public function modificarAlumno(Alumno $alumno)
            {
                
                var_dump($alumno);
                $retorno = '<h1> Modificar alumno</h1>';
                $retorno .= '<form action="?modulo=alumnomodificado&id=' . $alumno->getId() . '" method="post"> 
                             Nombre:<br>
                            <input type="text" name="nombre" value="'. $alumno->getNombre() .'"><br><br>
                            Apellidos:<br>
                            <input type="text" name="apellidos" value="'. $alumno->getApellidos() .'"><br><br>
                            Edad: <br>
                            <input type="text" name="edad" value="'. $alumno->getEdad() .'"><br><br>
                            <input type="submit" value="Modificar alumno">
                            <br>
                            </form>';
                echo $retorno;
                
            }
public function insertarAlumno()
            {
                
                
                $retorno = '<h1> Nuevo alumno</h1>';
                $retorno .= '<form action="?modulo=alumnoinsertado" method="post"> 
                             Nombre:<br>
                            <input type="text" name="nombre"><br><br>
                            Apellidos:<br>
                            <input type="text" name="apellidos"><br><br>
                            Edad: <br>
                            <input type="text" name="edad" ><br><br>
                            <input type="submit" value="Insertar alumno">
                            <br>
                            </form>';
                echo $retorno;
                
            }
}
