<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once('AsignaturaModelo.php');
/**
 * Description of Asignatura
 *
 * @author usuario
 */
class Asignatura {
    private $_id;
    private $_codigo;
    private $_nombreCorto;
    private $_nombreCompleto;
    
    public function __construct()
    {
        
    }
    
    public static function getAll()
    {
        $asignaturaModelo = new AsignaturaModelo;
        
        $asignaturas = $asignaturaModelo->getAll();
             
        return $asignaturas;
        
    }
    
    public function getAsignatura($id)
    {
        $asignaturaModelo = new AsignaturaModelo;
        $asignatura = $asignaturaModelo->getAsignatura($id);
        
        $this->_id=$asignatura['id'];
        $this->_codigo=$asignatura['codigo'];
        $this->_nombreCorto=$asignatura['nombreCorto'];
        $this->_nombreCompleto=$asignatura['nombreCompleto'];
    }
    
    function getId() {
        return $this->_id;
    }

    function getCodigo() {
        return $this->_codigo;
    }

    function getNombreCorto() {
        return $this->_nombreCorto;
    }

    function getNombreCompleto() {
        return $this->_nombreCompleto;
    }

    function setId($_id) {
        $this->_id = $_id;
        
    }

    function setCodigo($_codigo) {
        $this->_codigo = $_codigo;
        
    }

    function setNombreCorto($_nombreCorto) {
        $this->_nombreCorto = $_nombreCorto;
        
    }

    function setNombreCompleto($_nombreCompleto) {
        $this->_nombreCompleto = $_nombreCompleto;
       
    }
function modificar($nombreCompleto, $nombreCorto, $codigo)
    {
        $asignaturaModelo = new AsignaturaModelo;
        $asignaturaModelo->updateAsignatura($this->_id, $nombreCompleto, $nombreCorto, $codigo);
    }
    function borrar()
    {
        $alumnoModelo = new AlumnoModelo;
        $alumnoModelo->deleteAlumno($this->_id);
    }
function insertar($nombre, $apellidos, $edad)
    {
        $alumnoModelo = new AlumnoModelo;
        $alumnoModelo->insertAlumno($nombre, $apellidos, $edad);
    }

}
