<?php

require_once('AlumnoModelo.php');

class Alumno {
    private $_id;
    private $_nombre;
    private $_apellidos;
    private $_edad;
    
    public function __construct()
    {
        
    }
    
    
    public static function getAll()
    {
        $alumnoModelo = new AlumnoModelo;
        
        $alumnos = $alumnoModelo->getAll();
             
        return $alumnos;
        
    }
    
    public function getAlumno($id)
    {
        $alumnoModelo = new AlumnoModelo;
        $alumno = $alumnoModelo->getAlumno($id);
        
        $this->_id=$alumno['id'];
        $this->_nombre=$alumno['nombre'];
        $this->_apellidos=$alumno['apellidos'];
        $this->_edad=$alumno['edad'];
    }
    
    
    function getId()
    {
        return $this->_id;
    }
    
    function getNombre() 
    {
        return $this->_nombre;
    }
    

    function getApellidos() 
    {
        return $this->_apellidos;
    }

    function getEdad() 
    {
        return $this->_edad;
    }
    function setNombre($_nombre) {
        $this->_nombre = $_nombre;
    }

    function setApellidos($_apellidos) {
        $this->_apellidos = $_apellidos;
    }

    function setEdad($_edad) {
        $this->_edad = $_edad;
    }
    
    function modificar($nombre, $apellidos, $edad)
    {
        $alumnoModelo = new AlumnoModelo;
        $alumnoModelo->updateAlumno($this->_id, $nombre, $apellidos, $edad);
    }
    function borrar()
    {
        $alumnoModelo = new AlumnoModelo;
        $alumnoModelo->deleteAlumno($this->_id);
    }
function insertar($nombre, $apellidos, $edad)
    {
        $alumnoModelo = new AlumnoModelo;
        $alumnoModelo->insertAlumno($nombre, $apellidos, $edad);
    }

  
}
