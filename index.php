<?php
require_once ('Alumno.php');
require_once ('AlumnoVista.php');
require_once ('Asignatura.php');
require_once ('AsignaturaVista.php');
require_once ('LoginVista.php');
abstract class Index 
{
    public static function ejecutar()
    {
        
        //Si hay argumentos en GET procesamos el modulo correspondiente
        session_start();
        if(isset($_POST['usuario']) && isset($_POST['password'])){
            $_SESSION['usuario']=$_POST['usuario'];
            $_SESSION['password']=$_POST['password'];
        }
        
            
        if(isset($_SESSION['usuario'])){
            
          $_db = new mysqli('localhost', 'alumno', 'alumno', 'dweb');
            if ($_db->connect_errno > 0) {
                die("Imposible conectarse con la base de datos["
                        . $bbdd1->connect_error . "]");
            }
            $contador = 0;
            $sqlComprueba = 'SELECT * FROM usuarios';
            $resultadoSQL = $_db->query($sqlComprueba);
            $listaUsuarios = $resultadoSQL->fetch_all(MYSQLI_ASSOC);

            foreach ($listaUsuarios as $usuario) {
                if ($usuario['usuario'] == $_SESSION['usuario']) {
                    if ($usuario['password'] == md5($_SESSION['password'])) {
                        $contador = $contador + 1;
                    }
                }
            }  
            
        
        if ($contador > 0) {
        if (count($_GET)>0){
            self::_moduloBienvenido();
           echo '<a href="?" >Volver al inicio</a><br>'
            . '';
           
            self::_procesarModulo();
        }
        else{
            self::_moduloDefecto();
        }
        }else{
        session_destroy();
            echo 'USUARIO INCORRECTO. Introduzca de nuevo los datos:';
            self::_moduloLogin();
        }
    }else{
        
        self::_moduloLogin();
        
    }
    
    }
    
    private static function _moduloDefecto()
    {
        self::_moduloBienvenido();
       echo '<h3>Pagina de inicio</h3>';
       echo '<ul>';
       echo '<li><a href="?modulo=listado">Alumnos</a></li>';
       echo '<li><a href="?modulo=materias">Listado de materias</a></li>';
       echo '<li><a href="?modulo=borrarsesion">Cerrar sesion</a></li>';
       echo '</ul>';
    }
    
    private static function _moduloLogin()
    {
        LoginVista::mostrarLogin();
        
    }
    
    private static function _moduloBienvenido()
    {
         LoginVista::mostrarBienvenida();
    }
    
    private static function _noExisteModulo()
    {
        echo 'No existe el m&oacute;dulo';
    }
    
    private static function _procesarModulo()
    {
        switch($_GET['modulo']){
            case 'listado':
                echo '<b>TODO: listado de alumnos </b><br>';
                $alumnos = Alumno::getAll();
                AlumnoVista::mostrarListado($alumnos);
                break;
            case 'materias':
                echo '<b>TODO: Listado de materias </b><br>';
                $asignaturas = Asignatura::getAll();
                AsignaturaVista::mostrarListado($asignaturas);
                break;
            case 'nuevoalumno':
                echo 'TODO: formulario de alta de alumnos';
                AlumnoVista::insertarAlumno();
                break;
            case 'alumnoinsertado':
                echo 'TODO: Alumno subido con exito';
     
                $alumno=new Alumno();
                $alumno->insertar($_POST['nombre'],$_POST['apellidos'], $_POST['edad'] );
                
                header("Location:index.php?modulo=listado&mensaje=BORRADO");
                break;
            case 'detalle':
                echo 'TODO: Detalle del alumno';
                
                $id_alumno=$_GET['id'];
                
                $alumno=new Alumno();
                $alumno->getAlumno($id_alumno);
                //mostrarlo
                AlumnoVista::mostrarDetalle($alumno);
                
                break;
            case 'detalleMateria':
                echo 'TODO: Detalle e informacion de la asignatura';
                
                $id_asignatura=$_GET['id'];
                
                $asignatura=new Asignatura();
                $asignatura->getAsignatura($id_asignatura);
                //mostrarlo
                AsignaturaVista::mostrarDetalle($asignatura);
                
                break;
            case 'eliminar':
                echo 'TODO: Borrar alumno';
                $id_alumno=$_GET['id'];
                $alumno=new Alumno();
                $alumno->getAlumno($id_alumno);
                $alumno->borrar();
                header("Location:index.php?modulo=listado&mensaje=BORRADO");
                break;
            case 'modificaralumno':
              echo 'TODO: Modificacion de alumno';
                //recoger el id
                //crear objeto y cargarlo desde la BBDD
                //pedir a la vista que genere un formulario de modificacion
                $id_alumno=$_GET['id'];
                $alumno=new Alumno();
                $alumno->getAlumno($id_alumno);
                AlumnoVista::modificarAlumno($alumno);
                
                break;
            case 'modificarMateria':
              echo 'TODO: Modificacion de alumno';
                //recoger el id
                //crear objeto y cargarlo desde la BBDD
                //pedir a la vista que genere un formulario de modificacion
                $id_asignatura=$_GET['id'];
                $asignatura=new Asignatura();
                $asignatura->getAsignatura($id_asignatura);
                AsignaturaVista::modificarAsignatura($asignatura);
                
                break;
            case 'alumnomodificado': //alumno modificado
                echo 'TODO: ALUMNO MODIFICADO';
               $alumno = new Alumno();
               $alumno->getAlumno($_GET['id']);
               $alumno->modificar($_POST['nombre'],$_POST['apellidos'], $_POST['edad'] );
               
               header("Location:index.php?modulo=listado&mensaje=guardado");
              
              
                break;
            case 'asignaturamodificada':
                echo 'TODO: ASIGNATURA MODIFICADA';
                $asignatura = new Asignatura();
                $asignatura->getAsignatura($_GET['id']);
               $asignatura->modificar($_POST['nombreCompleto'],$_POST['nombreCorto'], $_POST['codigo'] );
               
               header("Location:index.php?modulo=materias&mensaje=guardado");
                break;
            case 'borrarsesion':
                session_destroy();
                echo '<h3> Saliendo de la sesion. </h3>';
                break;
            default:
                self::_noExisteModulo();
        }
    } 
}


Index::ejecutar();
