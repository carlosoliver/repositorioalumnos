<?php


class AlumnoModelo 
{
    const USER = 'alumno';
    const BBDD = 'dweb';
    const PSS = 'alumno';
    const HOST ='localhost';
    
    private $_db;
    
    public function __construct() 
    {
        $this->_db = new mysqli(self::HOST, self::USER, self::PSS, self::BBDD);
        if ($this->_db->connect_errno>0){
            die("Imposible conectarse con la base de datos["
                    . $bbdd1->connect_error."]");
        }
    }
    
    public function getAll()
    {
        if ($this->_db->connect_errno>0){
            die("Imposible conectarse con la base de datos["
                    . $bbdd1->connect_error."]");
        }else{
        $sql = 'SELECT * FROM alumnos';
        $resultado = $this->_db->query($sql);
        return $resultado->fetch_all(MYSQLI_ASSOC);
        }
    }
    
    public function getAlumno($id)
    {
        $sql = 'SELECT * FROM alumnos WHERE id='.$id;
        $resultado = $this->_db->query($sql);
        return $resultado->fetch_assoc();
    }
    
    public function updateAlumno($id, $nombre, $apellidos, $edad)
    {
        
        $sql = "UPDATE alumnos SET nombre='$nombre', apellidos='$apellidos', edad='$edad' WHERE id='$id'";
        if($this->_db->errno){
            echo $this->_db->error;
        }
       $resultado = $this->_db->query($sql);
            
    }
    public function deleteAlumno($id)
    {
        
        $sql = "DELETE FROM alumnos WHERE id='$id'";
        if($this->_db->errno){
            echo $this->_db->error;
        }
       $resultado = $this->_db->query($sql);
            
    }
    
    public function insertAlumno($nombre, $apellidos, $edad)
    {
       $sql = "INSERT INTO alumnos (nombre, apellidos, edad) VALUES ('$nombre', '$apellidos', '$edad')"; 
       $resultado = $this->_db->query($sql);
    }
}
